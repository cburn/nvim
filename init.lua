local fn = vim.fn
local execute = vim.api.nvim_command
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
vim.cmd 'filetype plugin on'
vim.api.nvim_set_keymap('n', '<Space>', '<NOP>', {noremap = true, silent = true})
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.o.termguicolors = true

-- vim.lsp.set_log_level("debug")


-- Sensible defaults
require('settings')


-- Install plugins
require('plugins')
require('plugins-config')

vim.cmd('map  <F2>  :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . \'> trans<\' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>')
require('lsp')

-- Key mappings
require('keymappings')
-- colorscheme
vim.cmd('colorscheme '.. 'lcars')
vim.opt.spelllang = 'en_us'
vim.opt.spell = true
