local get_hex = require("cokeline.utils").get_hex
local buffers = require("cokeline.buffers")
local fn = vim.fn
local colors = {
    bg = '#303030',
    yellow = '#DCDCAA',
    dark_yellow = '#D7BA7D',
    cyan = '#4EC9B0',
    mgold = '#CE9812',
    green = '#00ff58',
    light_green = '#B5CEA8',
    string_orange = '#CE9178',
    orange = '#FFAD00',
    purple = '#CF58CF',
    magenta = '#D16D9E',
    grey = '#B5C5CC',
    light_grey = '#D5D5DC',
    blue = '#569CD6',
    vivid_blue = '#77AAFF',
    light_blue = '#99ccff',
    red = '#D16969',
    error_red = '#FF5040',
    info_yellow = '#FFCC66'
}
local active_bg_color = colors.light_blue
local inactive_bg_color = colors.blue
local bg_color = colors.bg

function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

require('cokeline').setup({
      show_if_buffers_are_at_least = 1,
      mappings = {
          cycle_prev_next = true
      },
      default_hl = {
        fg = function()
          return colors.bg
        end,
        bg = function(buffer)
          if buffer.is_focused then
            return active_bg_color
          else
            return inactive_bg_color
          end
        end,
      },
      fill_hl = 'TabLineFill',
      components = {
          {
            text = function(buffer)
              local _text = ''
              if buffer.index > 1 then _text = ' ' end
              if buffer.is_focused or buffer.is_first then
                _text = _text .. ''
              end
              return _text
            end,
            fg = function(buffer)
              if buffer.is_focused then
                return active_bg_color
              elseif buffer.is_first then
                return inactive_bg_color
              end
            end,
            bg = function(buffer)
              if buffer.is_focused then
                if buffer.is_first then
                  return bg_color
                else
                  return inactive_bg_color
                end
              elseif buffer.is_first then
                  return bg_color
              end
            end
          },
          {
              text = function(buffer)
                  local status = ''
                  if buffer.is_readonly then
                      status = '➖'
                  elseif buffer.is_modified then
                      status = ''
                  end
                  return status
              end,
          },
          {
              text = function(buffer)
                return buffer.unique_prefix .. buffer.filename
              end,
              style = function(buffer)
                  local text_style = 'NONE'
                  if buffer.is_focused then
                      text_style = 'bold'
                  end
                  if buffer.diagnostics.errors > 0 then
                      if text_style ~= 'NONE' then
                          text_style = text_style .. ',underline'
                      else
                          text_style = 'underline'
                      end
                  end
                  return text_style
              end
          },
          {
              text = function(buffer)
                  local errors = buffer.diagnostics.errors
                  errors = ''
                  if buffer.diagnostics.errors > 0 then
                    errors = ''
                  end
                  return errors .. ' '
              end,
              fg = function(buffer)
                if buffer.diagnostics.errors == 0 then
                  return '#3DEB63'
                elseif buffer.diagnostics.errors <= 9 then
                  return '#DB121B'
                end
              end
          },
          {
              text = '',
              delete_buffer_on_left_click = true
          },
          {
            text = function(buffer)
              local next_buffer = buffers.get_valid_buffers()[buffer._valid_index + 1]
              if next_buffer ~= nil and next_buffer.is_focused then
                return ''
              elseif buffer.is_focused or buffer.is_last then
                return ''
              else
                return ''
              end
            end,
            fg = function(buffer)
              if buffer.number + 1 == fn.bufnr("%") then
                return active_bg_color
              elseif buffer.is_focused then
                return active_bg_color
              elseif buffer.is_last then
                return inactive_bg_color
              else
                return bg_color
              end
            end,
            bg = function(buffer)
              if buffer.number + 1 == fn.bufnr("%") then
                return active_bg_color
              elseif buffer.is_focused then
                if buffer.is_last then
                  return bg_color
                else
                  return inactive_bg_color
                end
              elseif buffer.is_last then
                  return bg_color
              end
            end
          }
      },
  })
