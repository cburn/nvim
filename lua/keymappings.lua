-- vim.api.nvim_set_keymap('n', '<Space>hs', '<NOP>', {noremap = true, silent = true})
local utils = require('utils')
utils.map('n', '<C-h>', '<cmd>noh<CR>') -- Clear highlights
utils.map('i', 'jk', '<Esc>')           -- jk to escape
utils.map('i', 'jj', '<Esc>')           -- jk to escape
utils.map('i', 'kj', '<Esc>')           -- jk to escape
vim.api.nvim_set_keymap("n", "<esc>", ":w<cr>", { noremap = true })

-- better indenting
vim.api.nvim_set_keymap('v', '<', '<gv', {noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '>', '>gv', {noremap = true, silent = true})

-- Move selected line / block of text in visual mode
vim.api.nvim_set_keymap('x', 'K', ':move \'<-2<CR>gv-gv', {noremap = true, silent = true})
vim.api.nvim_set_keymap('x', 'J', ':move \'>+1<CR>gv-gv', {noremap = true, silent = true})

-- Telescope keybinds

utils.map('n', '<leader>ff', '<cmd>Telescope find_files<cr>')
utils.map('n', '<leader>fr', '<cmd>Telescope lsp_references<cr>')
utils.map('n', '<leader>fc', '<cmd>Telescope lsp_code_actions<cr>')
utils.map('n', '<leader>fd', '<cmd>Telescope lsp_document_diagnostics<cr>')
utils.map('n', '<leader>fg', '<cmd>Telescope live_grep<cr>')
utils.map('n', '<leader>fb', '<cmd>Telescope buffer<cr>')
utils.map('n', '<leader>fh', '<cmd>Telescope help_tags<cr>')

utils.map('n', '<leader>e', '<cmd>:NvimTreeToggle<cr>')
-- make it easier w/ the file explorer
vim.api.nvim_set_keymap('n', '<A-h>', '<C-w>h', {silent = true})
vim.api.nvim_set_keymap('n', '<A-j>', '<C-w>j', {silent = true})
vim.api.nvim_set_keymap('n', '<A-k>', '<C-w>k', {silent = true})
vim.api.nvim_set_keymap('n', '<A-l>', '<C-w>l', {silent = true})

-- vim.api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-n>', {silent = false})
-- vim.api.nvim_set_keymap('t', '<C-h>', '<C-\\><C-n><C-w>h', {silent = true})
-- vim.api.nvim_set_keymap('t', '<C-j>', '<C-\\><C-n><C-w>j', {silent = true})
-- vim.api.nvim_set_keymap('t', '<C-k>', '<C-\\><C-n><C-w>k', {silent = true})
-- vim.api.nvim_set_keymap('t', '<C-l>', '<C-\\><C-n><C-w>l', {silent = true})

vim.api.nvim_set_keymap('i', '<A-h>', '<Esc><C-w>h', {silent = true})
vim.api.nvim_set_keymap('i', '<A-j>', '<Esc><C-w>j', {silent = true})
vim.api.nvim_set_keymap('i', '<A-k>', '<Esc><C-w>k', {silent = true})
vim.api.nvim_set_keymap('i', '<A-l>', '<Esc><C-w>l', {silent = true})

vim.api.nvim_set_keymap('i', '<C-h>', '<Left>', {silent = true})
-- vim.api.nvim_set_keymap('i', '<C-j>', '<Down>', {silent = true, expr = true})
-- vim.api.nvim_set_keymap('i', '<C-k>', '<Up>', {silent = true, expr = true})
vim.api.nvim_set_keymap('i', '<C-l>', '<Right>', {silent = true})

utils.map('n', '<leader>cf', '<cmd>call Black()<cr>')

utils.map('n', '<leader>cc', ':CommentToggle<cr>')
utils.map('v', '<leader>cc', ':CommentToggle<cr>')

-- HopPatern and chars doesnt work but the rest is good
utils.map('n', '<leader>hw', '<cmd>HopWord<cr>')
utils.map('n', '<leader>hl', '<cmd>HopLine<cr>')
vim.api.nvim_set_keymap('n', '<leader>hp', "<cmd>HopPattern<cr>", {})
utils.map('n', '<leader>h1', '<cmd>HopChar1<cr>')
utils.map('n', '<leader>h2', '<cmd>HopChar2<cr>')
utils.map('n', '<leader>ls', ':ls<cr>:b ')

vim.api.nvim_set_keymap('n', '<C-PageUp>',   '<Plug>(cokeline-focus-prev)', {silent = true})
vim.api.nvim_set_keymap('n', '<C-PageDown>', '<Plug>(cokeline-focus-next)', {silent = true})
vim.api.nvim_set_keymap('n', '<A-PageUp>',   '<Plug>(cokeline-switch-prev)', {silent = true})
vim.api.nvim_set_keymap('n', '<A-PageDown>', '<Plug>(cokeline-switch-next)', {silent = true})

vim.api.nvim_set_keymap('n', '<leader>dn', '<cmd>lua vim.diagnostic.goto_next()<CR>', {silent = true})
vim.api.nvim_set_keymap('n', '<leader>dp', '<cmd>lua vim.diagnostic.goto_prev()<CR>', {silent = true})

vim.api.nvim_set_keymap('n', '<leader>den', '<cmd>lua vim.diagnostic.goto_next({severity = vim.diagnostic.severity.ERROR})<CR>', {silent = true})
vim.api.nvim_set_keymap('n', '<leader>dep', '<cmd>lua vim.diagnostic.goto_prev({severity = vim.diagnostic.severity.ERROR})<CR>', {silent = true})

vim.cmd('set whichwrap+=<,>,[,]')

vim.api.nvim_set_keymap('n', '<leader>gf', ':e <cfile><cr>', {silent = true})

vim.api.nvim_set_keymap('i', '<C-right>', 'copilot#Accept("<CR>")', {expr=true, silent=true})
