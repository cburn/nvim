bc = require'lsp.base-config'
require'lspconfig'.pylsp.setup{
    on_attach = bc.on_attach,
    settings = {
        configurationSources = { "flake8", "black", "mypy", "isort"},
        plugins = {
        --   flake8 = { enabled = true },
        --   pyflakes = { enabled = false },
            pycodestyle = { enabled = false }
        }
    }
}

