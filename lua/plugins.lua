return require('lazy').setup({

    -- nvim reload for developing faster
    'famiu/nvim-reload',

    -- Tree sitter
    {'nvim-treesitter/nvim-treesitter', build = ':TSUpdate'},
    'p00f/nvim-ts-rainbow',
    'lukas-reineke/indent-blankline.nvim',
    'nvim-treesitter/playground',
    'JoosepAlviste/nvim-ts-context-commentstring',
    'windwp/nvim-ts-autotag',

    -- Color scheme stuff
    'cburnout/nvcode-color-schemes.vim',
    'norcalli/nvim-colorizer.lua',
    'sheerun/vim-polyglot',

    -- Fuzzy finder
    {
        'nvim-telescope/telescope.nvim',
        dependencies = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
    },
    -- 'mfussenegger/nvim-dap',

    -- icons
    'kyazdani42/nvim-web-devicons',
    'ryanoasis/vim-devicons',

    -- lsp
    'neovim/nvim-lspconfig',
    'neovim/nvim-lsp',
    -- 'glepnir/lspsaga.nvim',
    'onsails/lspkind-nvim',
    --'kosayoda/nvim-lightbulb',
    --'mfussenegger/nvim-jdtls',
    'RishabhRD/popfix',
    -- 'RishabhRD/nvim-lsputils',

    -- languages
    'JuliaEditorSupport/julia-vim',
    'omnisharp/omnisharp-vim',
    {'akinsho/flutter-tools.nvim', dependencies = 'nvim-lua/plenary.nvim'},

    -- git
    {
    'lewis6991/gitsigns.nvim',
    dependencies = {'nvim-lua/plenary.nvim'}
    },

    -- completion
   'hrsh7th/nvim-cmp' ,
   'hrsh7th/cmp-nvim-lsp',
   'hrsh7th/cmp-buffer',
   'hrsh7th/cmp-path',
   'hrsh7th/cmp-cmdline',
   {'tzachar/cmp-tabnine', build='./install.sh', dependencies = 'hrsh7th/nvim-cmp'},
   -- "hrsh7th/vim-vsnip",
   -- "rafamadriz/friendly-snippets",
   'L3MON4D3/LuaSnip',
   'saadparwaiz1/cmp_luasnip',
   "rafamadriz/friendly-snippets",
   'github/copilot.vim',

    -- vim dispatch
    'tpope/vim-dispatch',

    -- navigation
    'phaazon/hop.nvim',

    -- commenting
    'terrortylor/nvim-comment',

    -- Formatting
    'sbdchd/neoformat',
    -- python
    'averms/black-nvim',
    'stsewd/isort.nvim',
    -- Coq
    'whonore/Coqtail',

    -- paren pairing
    'windwp/nvim-autopairs',

    -- file explorer
    'kyazdani42/nvim-tree.lua',

    -- matching code bits
    'andymass/vim-matchup',

    -- statusline
    'feline-nvim/feline.nvim',

    -- bufferline
    {
      "willothy/nvim-cokeline",
      dependencies = {
        "nvim-lua/plenary.nvim",        -- Required for v0.4.0+
        "kyazdani42/nvim-web-devicons", -- If you want devicons
      },
      config = true
    },
    'kblin/vim-fountain',
    -- arduino stuff
    'stevearc/vim-arduino',
    --
    -- jupyter stuff
    { 'dccsillag/magma-nvim', build = ':UpdateRemotePlugins' },

})
